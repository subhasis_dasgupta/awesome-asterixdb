/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.util.Date;

public class ArticleType {

    private String content;
    private Date pubdate;
    // private String author; //reporter
    private AuthorType[] reporter;
    private String title;
    // private String keyword;
    private String[] keywords;
    private LocationNameType[] placeofreport;
    private OrganizationType[] organizations;
    private PersonType[] people;
    private String newspapersection = "";
    private String pagepicurl;
    private String pagenum;
    private String papername;
    private String page;
    private String file;
    private NewspaperType newspaper;

    public ArticleType() {
    	
    }
    public ArticleType(String title, String content, Date pubdate) {
	this.title = title;
	this.content = content;
	this.pubdate = pubdate;
    }

    public String getTitle() {
	return this.title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getContent() {
	return this.content;
    }

    public void setContent(String content) {
	this.content = content;
    }

    public Date getPubdate() {
	return this.pubdate;
    }

    public void setPubdate(Date pubdate) {
	this.pubdate = pubdate;
    }

    public AuthorType[] getReporter() {
    	return this.reporter;
    }

    public void setReporter(AuthorType[] reporter) {
    	this.reporter = reporter;
    }

    public LocationNameType[] getPlaceofreport() {
    	return this.placeofreport;
    }
    public void setPlaceofreport(LocationNameType[] placeofreport) {
    	this.placeofreport = placeofreport;
    }
    
    public OrganizationType[] getOrganizations() {
    	return this.organizations;
    }
    public void setOrganizations(OrganizationType[] organizations) {
    	this.organizations = organizations;
    }
    
    public PersonType[] getPeople() {
    	return this.people;
    }
    public void setPeople(PersonType[] people) {
    	this.people = people;
    }
    
    public String[] getKeywords() {
    	return this.keywords;
    }

    public void setKeywords(String[] keywords) {
    	this.keywords = keywords;
    }

    public String getPagepicurl() {
	return this.pagepicurl;
    }

    public void setPagepicurl(String pagepicurl) {
	this.pagepicurl = pagepicurl;
    }

    public String getPagenum() {
	return this.pagenum;
    }

    public void setPagenum(String pagenum) {
	this.pagenum = pagenum;
    }

    public String getPapername() {
	return this.papername;
    }

    public void setPapername(String papername) {
	this.papername = papername;
    }

    public String getPage() {
	return this.page;
    }

    public void setPage(String page) {
	this.page = page;
    }

    public String getFile() {
	return this.file;
    }

    public void setFile(String file) {
	this.file = file;
    }

    public void setNewspaper(NewspaperType newspaper) {
    	this.newspaper = newspaper;
    }
    public NewspaperType getNewspaper() {
    	return this.newspaper;
    }
    
    public void setNewspapersection(String newspapersection) {
    	this.newspapersection = newspapersection;
    }
    public String getNewspapersection() {
    	return this.newspapersection;
    }

    public String toString() {

	StringBuffer sb = new StringBuffer();
	sb.append("content: "+content+"\n");
	sb.append("title: "+title+"\n");
	sb.append("date: "+pubdate+"\n");
	sb.append("author: "+reporter[0].getName()+"\n");
	sb.append("keyword: "+keywords[0]+"\n");
	sb.append("pagepicurl: "+pagepicurl+"\n");
	sb.append("pagenum: "+pagenum+"\n");
	sb.append("papername: "+papername+"\n");
	sb.append("page: "+page+"\n");
	sb.append("file: "+file+"\n");
	return sb.toString();
	
    }
    
}


