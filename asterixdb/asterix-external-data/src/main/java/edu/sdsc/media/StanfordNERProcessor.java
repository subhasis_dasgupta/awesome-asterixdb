/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.util.Properties;
import java.util.logging.Logger;
import java.util.List;
import java.io.File;
import java.net.URL;

import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.*;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations;

public class StanfordNERProcessor {
	private static final Logger LOGGER = Logger.getLogger(StanfordNERProcessor.class.getName());
    //private static String HOME_DIR = "/Users/cyoun/asterix_eclipse/cna_news/cna_news_ingester";
    //private static String serializedClassifier = HOME_DIR+"/classifiers/stanford-ner-2012-11-11-chinese/chinese.misc.distsim.crf.ser.gz";
    //private static String basedir = HOME_DIR+"/stanford-segmenter-2014-08-27/data";
    private static String HOME_DIR;
    private static String serializedClassifier;
    private static String basedir;
    private static CRFClassifier<CoreLabel> segmenter;
    private static AbstractSequenceClassifier<CoreLabel> classifier; 

    static {

	try {
		//URL url = StanfordNERProcessor.class.getClassLoader().getResource("cna_news_ingester");
		HOME_DIR = System.getProperty("user.home") + File.separator + "cna_news_ingester";
		LOGGER.info("PATH: "+HOME_DIR);
		if (!new File(HOME_DIR).exists()) {
			try {
				throw new Exception ("PATH: "+HOME_DIR+" is not existed.");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		serializedClassifier = HOME_DIR+"/classifiers/stanford-ner-2012-11-11-chinese/chinese.misc.distsim.crf.ser.gz";
	    basedir = HOME_DIR+"/stanford-segmenter-2014-08-27/data";
		
	    // load chinese segmenter
	    Properties props = new Properties();
	    props.setProperty("sighanCorporaDict", basedir);
	    props.setProperty("serDictionary", basedir + "/dict-chris6.ser.gz");
	    props.setProperty("inputEncoding", "UTF-8");
	    props.setProperty("sighanPostProcessing", "true");
	    
	    segmenter = new CRFClassifier<CoreLabel>(props);
	    segmenter.loadClassifierNoExceptions(basedir + "/ctb.gz", props);

	    // load chinese entity recognizer
	    classifier = CRFClassifier.getClassifier(serializedClassifier);
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }


    static public String process(String text) {

	List<String> segmented = segmenter.segmentString(text);
	String result = null;
	for (String tmp : segmented) {
	    if (result == null) {
		result = tmp;
	    } else {
		result += " "+tmp;
	    }
	}

	String xml = classifier.classifyWithInlineXML(result);
	//System.out.println(xml);

	return xml;

    }


}

