/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.util.Map;
import java.util.logging.Logger;
import java.util.List;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.asterix.common.exceptions.AsterixException;
import org.apache.asterix.external.api.IExternalDataSourceFactory;
import org.apache.asterix.external.api.IRecordReader;
import org.apache.asterix.external.api.IRecordReaderFactory;
import org.apache.hyracks.algebricks.common.constraints.AlgebricksAbsolutePartitionConstraint;
import org.apache.hyracks.api.context.IHyracksTaskContext;
import org.apache.hyracks.api.exceptions.HyracksDataException;

public class MediaRecordReaderFactory implements IRecordReaderFactory<MediaDataRecord> {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(MediaRecordReaderFactory.class.getName());

    private static final int INTAKE_CARDINALITY = 1; // degree of parallelism at intake stage

    private final List<String> urls = new ArrayList<String>();
    private transient AlgebricksAbsolutePartitionConstraint clusterLocations;
    private Map<String, String> configuration;
    
    @Override
    public DataSourceType getDataSourceType() {
        return DataSourceType.RECORDS;
    }

    @Override
    public AlgebricksAbsolutePartitionConstraint getPartitionConstraint() {
    	int count = urls.size();
    	clusterLocations = IExternalDataSourceFactory.getPartitionConstraints(clusterLocations, count);
        //clusterLocations = IExternalDataSourceFactory.getPartitionConstraints(clusterLocations, INTAKE_CARDINALITY);
        return clusterLocations;
    }

    @Override
    public void configure(Map<String, String> configuration) throws AsterixException {
    	this.configuration = configuration;
    	if (!validateConfiguration(configuration)) {
            StringBuilder builder = new StringBuilder();
            builder.append("One or more parameters are missing from adapter configuration.\n");
            builder.append("Or date format string, yyyy-MM-dd is incorrect.\n");
            builder.append(MediaDataConstants.KEY_MEDIA_URL + "\n");
            builder.append(MediaDataConstants.KEY_START_DATE + "\n");
            builder.append(MediaDataConstants.KEY_END_DATE + "\n");
            throw new AsterixException(builder.toString());
        }
        String url = configuration.get(MediaDataConstants.KEY_MEDIA_URL);
        initializeURLs(url);
    }

    private void initializeURLs(String url) {
        urls.clear();
        String[] newspaperURLs = url.split(",");
        for (String newspaperURL : newspaperURLs) {
        	int idx = newspaperURL.indexOf(":");
    	    String name = newspaperURL.substring(0,idx).trim();
    	    try {
    	    	String classname = NewspaperClassNameEnum.valueOf(name).getClassname();
    	    	if (classname != null) {
    	    		urls.add(newspaperURL.trim());
    	    	}
    	    } catch (IllegalArgumentException e) {
        		StringBuilder builder = new StringBuilder();
                builder.append("The newspaper, "+newspaperURL.trim()+" is not supported.\n");
                throw new IllegalArgumentException(builder.toString());
        	}
        }
        //data management for unsaved data
        NotscrapingDataHandler ndh = new NotscrapingDataHandler();
        try {
			ndh.createDir();
		} catch (IOException e) {
			LOGGER.info("Can not create newspaper directory. "+e.getMessage());
		}
    }

    @Override
    public boolean isIndexible() {
        return false;
    }

    @Override
    public IRecordReader<? extends MediaDataRecord> createRecordReader(IHyracksTaskContext ctx, int partition) 
    		throws HyracksDataException {
        //IRecordReader<MediaDataRecord> reader;
        String startDate = configuration.get(MediaDataConstants.KEY_START_DATE);
        String endDate = configuration.get(MediaDataConstants.KEY_END_DATE);
        //return new MediaRecordReader(urls.get(partition));
        return new MediaRecordReader(urls.get(partition),startDate,endDate);
        //reader = new MediaRecordReader(urls);
        //return reader;
    }

    @Override
    public Class<? extends MediaDataRecord> getRecordClass() {
        return MediaDataRecord.class;
    }

    private boolean validateConfiguration(Map<String, String> configuration) {
        String url = configuration.get(MediaDataConstants.KEY_MEDIA_URL);
        String startDate = configuration.get(MediaDataConstants.KEY_START_DATE);
        String endDate = configuration.get(MediaDataConstants.KEY_END_DATE);
        if (url == null) {
            return false;
        } else if (url.trim().equals("")) return false;
        if ( startDate == null && endDate == null) return true;
        
        if (startDate != null && endDate != null) {
        	String format = "(([0-9]{4}-[0-9]{2})-([0-9]{2}))";
        	if (!startDate.matches(format)) return false;
        	if (!endDate.matches(format)) return false;
        	
        	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date start = formatter.parse(startDate);
				Date end = formatter.parse(endDate);
				if (start.equals(end)) return true;
	        	if (start.before(end)) return true;
			} catch (ParseException e) {
				e.printStackTrace();
			}
        }
        
        return false;
    }
}
