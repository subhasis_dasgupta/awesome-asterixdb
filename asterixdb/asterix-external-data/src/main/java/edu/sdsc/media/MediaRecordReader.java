/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Queue;

import org.apache.asterix.external.api.IRawRecord;
import org.apache.asterix.external.api.IRecordReader;
import org.apache.asterix.external.dataflow.AbstractFeedDataFlowController;
import org.apache.asterix.external.input.record.GenericRecord;
import org.apache.asterix.external.util.FeedLogManager;
import org.apache.log4j.Logger;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MediaRecordReader implements IRecordReader<MediaDataRecord> {

	private static final Logger LOGGER = Logger.getLogger(MediaRecordReader.class.getName());
    private boolean modified = false;
    private Queue<MediaDataRecord> mediaFeedBuffer = new ConcurrentLinkedQueue<MediaDataRecord>();
    private GenericRecord<MediaDataRecord> record = new GenericRecord<MediaDataRecord>();
    private boolean done = false;

    private String feedUrl;
    private List<String> feedUrls;
    private String start_date;
    private String end_date;
    private DateFormat formatter = new SimpleDateFormat("yyyy-MM/dd");
    private DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
 
    private NewspaperFeedThread hfdthread = null;

    public MediaRecordReader(String url,String start_date,String end_date) {
        feedUrl = url;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public MediaRecordReader(List<String> urls) {
        feedUrls = urls;
    }

    public MediaRecordReader(String url) {
        feedUrl = url;
    }
    
    public MediaRecordReader() {
        // empty constructor
    }

    @Override
    public void close() throws IOException {
    	//LOGGER.info("CLOSE...............................");
    }

    @Override
    public boolean hasNext() throws Exception {
        return !done;
    }

    @Override
    public IRawRecord<MediaDataRecord> next() throws IOException, InterruptedException {
        if (done) {
            return null;
        }
        try {
        	MediaDataRecord feedEntry = getNextFeed();
            if (feedEntry == null) {
                return null;
            }
            record.set(feedEntry);
            return record;
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    @Override
    public boolean stop() {
    	LOGGER.info("Stopping thread...............");
        if(hfdthread != null) {
               	LOGGER.info("Stopping thread: " + hfdthread);
                hfdthread.interrupt();
                hfdthread.stopFlag();
                LOGGER.info("Thread successfully stopped.");
        }
    	done = true;
        return done;
    }

    private MediaDataRecord getNextFeed() throws Exception {
        if (mediaFeedBuffer.isEmpty() && !modified) {
            fetchFeed();
        }
        if (mediaFeedBuffer.isEmpty()) {
            return null;
        } else {
            return mediaFeedBuffer.remove();
        }
    }

    private void fetchFeed() throws Exception {
    	hfdthread  = new NewspaperFeedThread(feedUrl,start_date,end_date,mediaFeedBuffer);                                                                                      
        hfdthread.start();
    	modified = true;
    	LOGGER.info(feedUrl+"==> Background process successfully started.");
    }
    
    @Override
    public void setFeedLogManager(FeedLogManager feedLogManager) {
    }

    @Override
    public void setController(AbstractFeedDataFlowController controller) {
    }
    
    @Override
    public boolean handleException(Throwable th) {
        return false;
    }
    
}
