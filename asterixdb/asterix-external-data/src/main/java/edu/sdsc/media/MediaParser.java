/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import org.apache.asterix.builders.RecordBuilder;
import org.apache.asterix.external.api.IDataParser;
import org.apache.asterix.external.api.IRawRecord;
import org.apache.asterix.external.api.IRecordDataParser;
import org.apache.asterix.om.base.AMutableInt64;
import org.apache.asterix.om.base.AMutableRecord;
import org.apache.asterix.om.base.AMutableString;
import org.apache.asterix.om.base.AMutableUnorderedList;
import org.apache.asterix.om.base.AMutableOrderedList;
import org.apache.asterix.om.base.ABoolean;
import org.apache.asterix.om.types.AOrderedListType;
import org.apache.asterix.om.types.AUnorderedListType;
import org.apache.asterix.om.types.BuiltinType;
import org.apache.asterix.om.base.AMutableDate;
import org.apache.asterix.om.base.IAObject;
import org.apache.asterix.om.base.temporal.GregorianCalendarSystem;
import org.apache.asterix.om.types.ARecordType;
import org.apache.asterix.om.types.ATypeTag;
import org.apache.asterix.om.types.IAType;

import edu.sdsc.media.MediaDataConstants.MediaDataType;
import edu.sdsc.media.MediaDataRecord;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MediaParser implements IRecordDataParser<MediaDataRecord> {
	private static final Logger LOGGER = Logger.getLogger(MediaParser.class.getName());
	//private final static long CHRONON_OF_DAY = 24 * 60 * 60 * 1000;
	private final GregorianCalendarSystem CAL = GregorianCalendarSystem.getInstance();
    private final Map<String, Integer> articleFieldNameMap = new HashMap<>();
    // private final Map<String, Integer> dailynewsFieldNameMap = new HashMap<>();
    private final Map<String, Integer> newspaperFieldNameMap = new HashMap<>();
    private final Map<String, Integer> authorFieldNameMap = new HashMap<>();
    private final Map<String, Integer> locationFieldNameMap = new HashMap<>();
    private final Map<String, Integer> organizationFieldNameMap = new HashMap<>();
    private final Map<String, Integer> personFieldNameMap = new HashMap<>();

    private ARecordType recordType;
    private IAType locationrecordType;
    private IAType dnrecordType;
    // private IAType articlerecordType;
    private IAType authorrecordType;
    private IAType newspaperrecordType;
    private IAType orgrecordType;
    private IAType personrecordType;
    
    private RecordBuilder recordBuilder = new RecordBuilder();
    
	private int index_author = 0;
	private int index_place = 0;
	private int index_newstype = 0;
	private int index_org = 0;
	private int index_person = 0;

	public MediaParser(ARecordType recordType) {
		this.recordType = recordType;
		initFieldNames(recordType);
		
		index_author = articleFieldNameMap.get(MediaDataType.REPORTER);
    	index_place = articleFieldNameMap.get(MediaDataType.PLACE);
    	index_newstype = articleFieldNameMap.get(MediaDataType.NEWSTYPE);
    	index_org = articleFieldNameMap.get(MediaDataType.ORGANIZATION);
    	index_person = articleFieldNameMap.get(MediaDataType.PERSON);
    	newspaperrecordType = recordType.getFieldTypes()[index_newstype];
    	
    	IAType authorfieldType = recordType.getFieldTypes()[index_author];
    	authorrecordType = ((AUnorderedListType) authorfieldType).getItemType();

    	IAType locationfieldType = recordType.getFieldTypes()[index_place];
    	locationrecordType = ((AOrderedListType) locationfieldType).getItemType();

    	IAType orgfieldType = recordType.getFieldTypes()[index_org];
    	orgrecordType = ((AUnorderedListType) orgfieldType).getItemType();

    	IAType personfieldType = recordType.getFieldTypes()[index_person];
    	personrecordType = ((AUnorderedListType) personfieldType).getItemType();
	}
    
    // Initialize the hashmap values for the field names and positions
    private void initFieldNames(ARecordType recordType) {
        String articleFields[] = recordType.getFieldNames();
        for (int i = 0; i < articleFields.length; i++) {
        	articleFieldNameMap.put(articleFields[i], i);
        	// System.out.println("article: "+i+":"+articleFields[i]);
        	if (articleFields[i].equals(MediaDataType.REPORTER)) {
            	//authorType
            	IAType authorfieldType = recordType.getFieldTypes()[i];
                if (authorfieldType.getTypeTag() == ATypeTag.UNORDEREDLIST) {
                	IAType aurecord = ((AUnorderedListType) authorfieldType).getItemType();
                	String authorFields[] = ((ARecordType) aurecord).getFieldNames();
                    for (int l = 0; l < authorFields.length; l++) {
                        authorFieldNameMap.put(authorFields[l], l);
                        // System.out.println("author type: "+l+":"+authorFields[l]);
                    }
                }
        	} else if (articleFields[i].equals(MediaDataType.ORGANIZATION)) {
            	//organizationType
            	IAType orgfieldType = recordType.getFieldTypes()[i];
                if (orgfieldType.getTypeTag() == ATypeTag.UNORDEREDLIST) {
                	IAType orgrecord = ((AUnorderedListType) orgfieldType).getItemType();
                	String orgFields[] = ((ARecordType) orgrecord).getFieldNames();
                    for (int l = 0; l < orgFields.length; l++) {
                        organizationFieldNameMap.put(orgFields[l], l);
                        // System.out.println("org type: "+l+":"+orgFields[l]);
                    }
                }
        	} else if (articleFields[i].equals(MediaDataType.PERSON)) {
            	//organizationType
            	IAType personfieldType = recordType.getFieldTypes()[i];
                if (personfieldType.getTypeTag() == ATypeTag.UNORDEREDLIST) {
                	IAType personrecord = ((AUnorderedListType) personfieldType).getItemType();
                	String personFields[] = ((ARecordType) personrecord).getFieldNames();
                    for (int l = 0; l < personFields.length; l++) {
                        personFieldNameMap.put(personFields[l], l);
                        // System.out.println("person type: "+l+":"+personFields[l]);
                    }
                }
        	} else if (articleFields[i].equals(MediaDataType.NEWSTYPE)) {
                IAType fieldType = recordType.getFieldTypes()[i];
                if (fieldType.getTypeTag() == ATypeTag.RECORD) {
                    String newspaperFields[] = ((ARecordType) fieldType).getFieldNames();
                    for (int j = 0; j < newspaperFields.length; j++) {
                    	newspaperFieldNameMap.put(newspaperFields[j], j);
                    	// System.out.println("newspaper type: "+j+":"+newspaperFields[j]);
                    	if (newspaperFields[j].equals(MediaDataType.LOCATION)) {
                    		//LocationNameType
                    		IAType locfieldType = ((ARecordType) fieldType).getFieldTypes()[j];
                    		if (locfieldType.getTypeTag() == ATypeTag.ORDEREDLIST) {
                    			IAType lnrecord = ((AOrderedListType) locfieldType).getItemType();
                    			String locationFields[] = ((ARecordType) lnrecord).getFieldNames();
                    			for (int k = 0; k < locationFields.length; k++) {
                    				locationFieldNameMap.put(locationFields[k], k);
                    				// System.out.println("location type: "+k+":"+locationFields[k]);
                    			}
                    		}
                    	}
                    }
                }
        	}
        }    
    }

    private AMutableRecord getAuthorRecord() {
        AOrderedListType specialityType = new AOrderedListType(BuiltinType.ASTRING,MediaDataType.AOLIST_SPECIALITY);
        IAObject[] AuthorFields = new IAObject[] { new AMutableString(null),
    			new AMutableOrderedList(specialityType) };
    	return new AMutableRecord((ARecordType) authorrecordType,AuthorFields);  	   	   	
    }

    private AMutableRecord getLocationRecord() {
    	IAObject[] LocationFields = new IAObject[] { new AMutableString(null), new AMutableString(null), 
    			ABoolean.FALSE, new AMutableInt64(0), new AMutableInt64(0) };
    	return new AMutableRecord((ARecordType) locationrecordType,LocationFields);  	
    }
 
    private AMutableRecord getOrganizationRecord() {
    	IAObject[] orgFields = new IAObject[] { new AMutableString(null), ABoolean.FALSE, 
    			new AMutableInt64(0), new AMutableInt64(0) };
    	return new AMutableRecord((ARecordType) orgrecordType, orgFields);  	
    }

    private AMutableRecord getPersonRecord() {
    	IAObject[] personFields = new IAObject[] { new AMutableString(null), ABoolean.FALSE, 
    			new AMutableInt64(0), new AMutableInt64(0) };
    	return new AMutableRecord((ARecordType) personrecordType, personFields);  	
    }

    private AMutableRecord getArticleRecord() {
    	AMutableRecord AuthorRecord = getAuthorRecord();
    	AUnorderedListType unorder_author = new AUnorderedListType(AuthorRecord.getType(),MediaDataType.AULIST_REPORTER);
    	AMutableUnorderedList Author = new AMutableUnorderedList(unorder_author);
 
    	AMutableRecord OrgRecord = getOrganizationRecord();
    	AUnorderedListType unorder_org = new AUnorderedListType(OrgRecord.getType(),MediaDataType.AULIST_ORG);
    	AMutableUnorderedList Organization = new AMutableUnorderedList(unorder_org);

    	AMutableRecord PersonRecord = getPersonRecord();
    	AUnorderedListType unorder_person = new AUnorderedListType(PersonRecord.getType(),MediaDataType.AULIST_PERSON);
    	AMutableUnorderedList People = new AMutableUnorderedList(unorder_person);

    	AMutableRecord PlaceRecord = getLocationRecord();
    	AUnorderedListType keywordType = new AUnorderedListType(BuiltinType.ASTRING,MediaDataType.AULIST_KEYWORD);
    	AOrderedListType order_place = new AOrderedListType(PlaceRecord.getType(),MediaDataType.AOLIST_PLACE);
    	AMutableOrderedList Place = new AMutableOrderedList(order_place);

    	AMutableRecord newsRecord = getNewspaperRecord();
    	IAObject[] ArticleFields = new IAObject[] { new AMutableString(null), new AMutableString(null),
    			new AMutableDate(0), newsRecord,
        		Author, new AMutableUnorderedList(keywordType), Place, Organization, People, 
        		new AMutableString(null), new AMutableString(null), new AMutableString(null),
        		new AMutableString(null), new AMutableString(null), new AMutableString(null) };
        
        return new AMutableRecord(recordType,ArticleFields);
    }
    
    /*
    private AMutableRecord getDailynewsRecord() {
    	AMutableRecord ArticleRecord = getArticleRecord();
    	AOrderedListType order_articles = new AOrderedListType(ArticleRecord.getType(),MediaDataType.AOLIST_ARTICLE);
    	AMutableOrderedList Article = new AMutableOrderedList(order_articles);
        
    	IAObject[] DailynewsFields = new IAObject[] { new AMutableInt64(0), new AMutableDate(0), 
        		Article};
        return new AMutableRecord((ARecordType) dnrecordType,DailynewsFields);
    }
    */
    
    private AMutableRecord getNewspaperRecord() {
    	//AMutableRecord DailynewsRecord = getDailynewsRecord();
        //AOrderedListType order_dailynews = new AOrderedListType(DailynewsRecord.getType(),MediaDataType.AOLIST_DAILYNEWS);
        //AMutableOrderedList Dailynews = new AMutableOrderedList(order_dailynews);

        AMutableRecord PlaceRecord = getLocationRecord();
    	AOrderedListType order_place = new AOrderedListType(PlaceRecord.getType(),MediaDataType.AOLIST_PLACE);
    	AMutableOrderedList Place = new AMutableOrderedList(order_place);

    	IAObject[] NewspaperFields = new IAObject[] { new AMutableInt64(0), new AMutableString(null), 
    			Place, ABoolean.FALSE,ABoolean.FALSE,ABoolean.FALSE,
        		new AMutableString(null), new AMutableDate(0), new AMutableDate(0)};
    	
    	return new AMutableRecord((ARecordType) newspaperrecordType, NewspaperFields);
    }
       
    private AMutableRecord getLocationDataRecord(LocationNameType lnt) {
        // LocationNameType
    	int index_lnname = locationFieldNameMap.get(MediaDataType.LN_NAME);
    	int index_lntype = locationFieldNameMap.get(MediaDataType.LN_TYPE);
    	int index_lnino = locationFieldNameMap.get(MediaDataType.LN_INO);
    	int index_lnonid = locationFieldNameMap.get(MediaDataType.LN_ONID);
    	int index_lnoneid = locationFieldNameMap.get(MediaDataType.LN_ONEID);

    	AMutableRecord locRecord = getLocationRecord();        	
    	((AMutableString) locRecord.getValueByPos(index_lnname)).setValue(lnt.getName());
    	((AMutableString) locRecord.getValueByPos(index_lntype)).setValue(lnt.getLocationtype());
    	if (lnt.isInontology())
    		locRecord.setValueAtPos(index_lnino, ABoolean.TRUE);
    	else
    		locRecord.setValueAtPos(index_lnino, ABoolean.FALSE);
    	((AMutableInt64) locRecord.getValueByPos(index_lnonid)).setValue(lnt.getOntologyid());
    	((AMutableInt64) locRecord.getValueByPos(index_lnoneid)).setValue(lnt.getOntologyid());

    	return locRecord;
    }
    
    private AMutableRecord getOrganizationDataRecord(OrganizationType org) {
    	// OrganizationType
    	int index_orgname = organizationFieldNameMap.get(MediaDataType.ORG_NAME);
    	int index_orgino = organizationFieldNameMap.get(MediaDataType.ORG_INO);
    	int index_orgonid = organizationFieldNameMap.get(MediaDataType.ORG_ONID);
    	int index_orgoneid = organizationFieldNameMap.get(MediaDataType.ORG_ONEID);
    	
    	AMutableRecord orgRecord = getOrganizationRecord();
		((AMutableString) orgRecord.getValueByPos(index_orgname)).setValue(org.getName());
		if (org.isInontology())
			orgRecord.setValueAtPos(index_orgino, ABoolean.TRUE);
		else
			orgRecord.setValueAtPos(index_orgino, ABoolean.FALSE);
		((AMutableInt64) orgRecord.getValueByPos(index_orgonid)).setValue(org.getOntologyid());
		((AMutableInt64) orgRecord.getValueByPos(index_orgoneid)).setValue(org.getOntologyentityid());

		return orgRecord;
    }
    
    private AMutableRecord getPersonDataRecord(PersonType per) {
    	// PersonType
    	int index_pername = personFieldNameMap.get(MediaDataType.PER_NAME);
    	int index_perino = personFieldNameMap.get(MediaDataType.PER_INO);
    	int index_peronid = personFieldNameMap.get(MediaDataType.PER_ONID);
    	int index_peroneid = personFieldNameMap.get(MediaDataType.PER_ONEID);

		AMutableRecord personRecord = getPersonRecord();
		((AMutableString) personRecord.getValueByPos(index_pername)).setValue(per.getName());
		if (per.isInontology())
			personRecord.setValueAtPos(index_perino, ABoolean.TRUE);
		else
			personRecord.setValueAtPos(index_perino, ABoolean.FALSE);
		((AMutableInt64) personRecord.getValueByPos(index_peronid)).setValue(per.getOntologyid());
		((AMutableInt64) personRecord.getValueByPos(index_peroneid)).setValue(per.getOntologyentityid());

		return personRecord;
    }
    
    private AMutableRecord getAuthorDataRecord(AuthorType author) {
        // AuthorType
    	int index_auname = authorFieldNameMap.get(MediaDataType.AUTHOR_NAME);
    	int index_auarea = authorFieldNameMap.get(MediaDataType.AUTHOR_AREA);

		AMutableRecord authorRecord = getAuthorRecord();
		String authorName = author.getName();
		if (authorName != null && !authorName.trim().equals("")) {
			((AMutableString) authorRecord.getValueByPos(index_auname)).setValue(authorName.trim());
			String speciality[] = author.getSpecialty();
			if (speciality != null) {
				for (String s: speciality) {
					((AMutableOrderedList) authorRecord.getValueByPos(index_auarea)).add(new AMutableString(s));
				}
			}
		} else {
			return null;
		}
		return authorRecord;
    }
    
    private AMutableRecord getNewspaperDataRecord(NewspaperType newstype) {
    	
        //get indices for NewspaperType
    	int index_loc = newspaperFieldNameMap.get(MediaDataType.LOCATION);
    	int index_newsid = newspaperFieldNameMap.get(MediaDataType.NEWSPAPER_ID);
    	int index_newsname = newspaperFieldNameMap.get(MediaDataType.NEWSPAPER_NAME);
    	int index_isc = newspaperFieldNameMap.get(MediaDataType.IS_COMMERCIAL);
    	int index_isl = newspaperFieldNameMap.get(MediaDataType.IS_LOCAL);
    	int index_isf = newspaperFieldNameMap.get(MediaDataType.IS_FEDERAL);
    	int index_cir = newspaperFieldNameMap.get(MediaDataType.CIRCULATION);
    	int index_sdate = newspaperFieldNameMap.get(MediaDataType.START_DATE);
    	int index_edate = newspaperFieldNameMap.get(MediaDataType.END_DATE);
    	// int index_dailynews = newspaperFieldNameMap.get(MediaDataType.DAILYNEWS);
    	
    	AMutableRecord newspaperRecord = getNewspaperRecord();
    	//((AMutableOrderedList) mutableNewspaperFields[index_loc]).setValues(new ArrayList<IAObject>());
    	//((AMutableOrderedList) newspaperRecord.getValueByPos(index_loc)).setValues(new ArrayList<IAObject>());
    	//((AMutableOrderedList) mutableNewspaperFields[index_dailynews]).setValues(new ArrayList<IAObject>());
    	//((AMutableOrderedList) newspaperRecord.getValueByPos(index_dailynews)).setValues(new ArrayList<IAObject>());
        //((AMutableInt64) mutableNewspaperFields[index_newsid]).setValue(article_id);
    	((AMutableInt64) newspaperRecord.getValueByPos(index_newsid)).setValue(newstype.getNewspaperid());
        //((AMutableString) mutableNewspaperFields[index_newsname]).setValue(entry.getNewspapername());
    	((AMutableString) newspaperRecord.getValueByPos(index_newsname)).setValue(newstype.getNewspapername());
    	LocationNameType locs[] = newstype.getLocation();
        for (LocationNameType s: locs) {
        	((AMutableOrderedList) newspaperRecord.getValueByPos(index_loc)).add(getLocationDataRecord(s));
        }

        if (newstype.isIscommercial())
        	newspaperRecord.setValueAtPos(index_isc, ABoolean.TRUE);
        else
        	newspaperRecord.setValueAtPos(index_isc, ABoolean.FALSE);
        
        if (newstype.isIsfederalgovernmentissued())
        	newspaperRecord.setValueAtPos(index_isf, ABoolean.TRUE);
        else
        	newspaperRecord.setValueAtPos(index_isf, ABoolean.FALSE);
        
        if (newstype.isIslocalgovernmentissued())
        	newspaperRecord.setValueAtPos(index_isl, ABoolean.TRUE);
        else
        	newspaperRecord.setValueAtPos(index_isl, ABoolean.FALSE);

        ((AMutableString) newspaperRecord.getValueByPos(index_cir)).setValue(newstype.getCirculationlevel());
        //long sdate = newstype.getStartdate().getTime();
        //long edate = newstype.getEnddate().getTime();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(newstype.getStartdate());
        long sdate = CAL.getChronon(calendar.get(Calendar.YEAR),
        		calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH),
        		0,0,0,0,0);
        calendar = new GregorianCalendar();
        calendar.setTime(newstype.getEnddate());
        long edate = CAL.getChronon(calendar.get(Calendar.YEAR),
        		calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH),
        		0,0,0,0,0);
		//int isdate = (int) (sdate / CHRONON_OF_DAY);
        //int iedate = (int) (edate / CHRONON_OF_DAY);
        int isdate = (int) (sdate / GregorianCalendarSystem.CHRONON_OF_DAY);
        int iedate = (int) (edate / GregorianCalendarSystem.CHRONON_OF_DAY);		
        ((AMutableDate) newspaperRecord.getValueByPos(index_sdate)).setValue(isdate);
        ((AMutableDate) newspaperRecord.getValueByPos(index_edate)).setValue(iedate);
        
        return newspaperRecord;

    }
    
    private AMutableRecord getArticleDataRecord(ArticleType entry) {
    	//get indices for ArticleType
    	int index_articleid = articleFieldNameMap.get(MediaDataType.ARTICLE_ID);
    	int index_title = articleFieldNameMap.get(MediaDataType.TITLE);
    	int index_pubdate = articleFieldNameMap.get(MediaDataType.PUBDATE);
    	//int index_author = articleFieldNameMap.get(MediaDataType.REPORTER);
    	int index_keyword = articleFieldNameMap.get(MediaDataType.KEYWORDS);
    	//int index_place = articleFieldNameMap.get(MediaDataType.PLACE);
    	int index_section = articleFieldNameMap.get(MediaDataType.SECTION);
    	int index_pagepicurl = articleFieldNameMap.get(MediaDataType.PAGEPICURL);
    	int index_pagenum = articleFieldNameMap.get(MediaDataType.PAGENUM);
    	int index_page = articleFieldNameMap.get(MediaDataType.PAGE);
    	int index_file = articleFieldNameMap.get(MediaDataType.FILE);
    	int index_content = articleFieldNameMap.get(MediaDataType.CONTENT);
    	//int index_newstype = articleFieldNameMap.get(MediaDataType.NEWSTYPE);
    	//int index_org = articleFieldNameMap.get(MediaDataType.ORGANIZATION);
    	//int index_person = articleFieldNameMap.get(MediaDataType.PERSON);
    	
    	NewspaperType newstype = entry.getNewspaper();
    	AMutableRecord newspaperRecord = getNewspaperDataRecord(newstype);
        AMutableRecord articleRecord = getArticleRecord();
        articleRecord.setValueAtPos(index_newstype, newspaperRecord); // .getValueByPos(index_newstype)).setValueAtPos(index_newstype, newspaperRecord);
        Date pubdate = entry.getPubdate();
    	//long pub_date = pubdate.getTime();
    	//int days = (int) (pub_date / CHRONON_OF_DAY);
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(pubdate);
        long pub_date = CAL.getChronon(calendar.get(Calendar.YEAR),
        		calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH),
        		0,0,0,0,0);
    	int days = (int) (pub_date / GregorianCalendarSystem.CHRONON_OF_DAY);
    	((AMutableDate) articleRecord.getValueByPos(index_pubdate)).setValue(days);
    	
    	//System.out.print("Article ID: "+article_id);
    	//LOGGER.info("Title: "+entry.getTitle());
    	String articleid = UUID.randomUUID().toString();
    	((AMutableString) articleRecord.getValueByPos(index_articleid)).setValue(articleid);
    	((AMutableString) articleRecord.getValueByPos(index_title)).setValue(entry.getTitle());
    	//((AMutableUnorderedList) articleRecord.getValueByPos(index_author)).clear();
    	//((AMutableUnorderedList) articleRecord.getValueByPos(index_keyword)).clear();
    	        
    	//reporters
    	AuthorType authors[] = entry.getReporter();
    	if (authors != null) {
    		for (AuthorType a: authors) {
    			AMutableRecord authorRecord = getAuthorDataRecord(a);
    			if (authorRecord != null)
    				((AMutableUnorderedList) articleRecord.getValueByPos(index_author)).add(authorRecord);
    		}
    	}
    	
    	//keywords
    	String[] keywords = entry.getKeywords();
    	if (keywords != null) {
    		for (String s: keywords) {
    			if (s != null && s.trim().length() > 0)
    				((AMutableUnorderedList) articleRecord.getValueByPos(index_keyword)).add(new AMutableString(s));
    		}	
    	}
    	
    	//place_of_report
    	LocationNameType por[] = entry.getPlaceofreport();
    	if (por != null) {
    		for (LocationNameType p: por) {
    			((AMutableOrderedList) articleRecord.getValueByPos(index_place)).add(getLocationDataRecord(p));
    		}
    	}
    	
    	OrganizationType orgs[] = entry.getOrganizations();
    	if (orgs != null) {
    		for (OrganizationType o: orgs) {
    			((AMutableUnorderedList) articleRecord.getValueByPos(index_org)).add(getOrganizationDataRecord(o));
    		}
    	}

    	PersonType people[] = entry.getPeople();
    	if (people != null) {
    		for (PersonType p: people) {
    			((AMutableUnorderedList) articleRecord.getValueByPos(index_person)).add(getPersonDataRecord(p));
    		}
    	}

    	String pagepicurl = (entry.getPagepicurl() == null) ? "" : entry.getPagepicurl();
    	String pagenum = (entry.getPagenum() == null) ? "" : entry.getPagenum();
    	String nssection = (entry.getNewspapersection() == null) ? "" : entry.getNewspapersection();
    	String page = (entry.getPage() == null) ? "" : entry.getPage();
    	String file = (entry.getFile() == null) ? "" : entry.getFile();
    	((AMutableString) articleRecord.getValueByPos(index_pagepicurl)).setValue(pagepicurl);
    	((AMutableString) articleRecord.getValueByPos(index_pagenum)).setValue(pagenum);
    	((AMutableString) articleRecord.getValueByPos(index_section)).setValue(nssection);
    	((AMutableString) articleRecord.getValueByPos(index_page)).setValue(page);
    	((AMutableString) articleRecord.getValueByPos(index_file)).setValue(file);
    	
    	String content = entry.getContent();
        //int size = content.length();
        //LOGGER.info("Content Size: "+size);
    	((AMutableString) articleRecord.getValueByPos(index_content)).setValue(content);
    	
    	return articleRecord;
    }
      
    @Override
    public void parse(IRawRecord<? extends MediaDataRecord> record, DataOutput out) throws IOException {
    	MediaDataRecord newsrecord = record.get();
    	ArticleType entry = newsrecord.getArticle();
    	    	
    	AMutableRecord articleRecord = getArticleDataRecord(entry);
        recordBuilder.reset(articleRecord.getType());
        recordBuilder.init();
        IDataParser.writeRecord(articleRecord, out, recordBuilder);
    }

}
