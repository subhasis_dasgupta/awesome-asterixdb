/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

public class DateIterator implements Iterator<Date>, Iterable<Date>{

    private Calendar end = Calendar.getInstance();
    private Calendar current = Calendar.getInstance();

    public DateIterator(Date start, Date end){
	this.end.setTime(end);
	this.end.add(Calendar.DATE, -1);
	this.current.setTime(start);
	this.current.add(Calendar.DATE, -1);
    }

    public boolean hasNext(){
        return !current.after(end);
    }

    public Date next(){
        current.add(Calendar.DATE, 1);
        return current.getTime();
    }

    public void remove(){
        throw new UnsupportedOperationException("Cannot remove");
    }

    public Iterator<Date> iterator(){
        return this;
    }

    public static void main(String[] args){
	Date d1 = new Date();
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, 20);
	Date d2 = cal.getTime();

	Iterator<Date> i = new DateIterator(d1, d2);
	while(i.hasNext()){
	    Date date = i.next();
	    System.out.println(date);
	}
    }
} 
