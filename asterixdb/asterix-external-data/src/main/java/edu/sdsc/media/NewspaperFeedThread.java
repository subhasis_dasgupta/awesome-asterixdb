/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class NewspaperFeedThread extends Thread {
	private static final Logger LOGGER = Logger.getLogger(NewspaperFeedThread.class.getName());
	private final Queue<MediaDataRecord> mediaFeedBuffer;
	private String name;
	private String classname;
	private volatile boolean exit = false;
	// private final String feedUrl;
	  
	private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	private boolean dirFlag = true;
	private boolean todayFlag = true;
	private String start_date;
	private String end_date;
	private NewspaperType newspaperRecord;
	NotscrapingDataHandler nsdhandler = new NotscrapingDataHandler();
	//private Map<String, Integer> unscrapingdata = new HashMap<String, Integer>();
	private Map<String, Integer> unscrapingdata = new ConcurrentHashMap<String, Integer>();
	private Object newsObject;
	private Date today;

	public NewspaperFeedThread(
			String feedUrl, 
			String start_date, 
			String end_date,
			Queue<MediaDataRecord> mediaFeedBuffer) {
		setNewsfeedObject(feedUrl);
		this.start_date = start_date;
		this.end_date = end_date;
		this.mediaFeedBuffer = mediaFeedBuffer;
	  }
	  	  	 
	@Override 
	public synchronized void run() {
		initNewsFeedData();
		if (exit) {
			saveData();
			LOGGER.info(name+" Server is stooped.....");
			return;
		}
		
	    while (!Thread.currentThread().isInterrupted() && !exit) {
	    	try {
	    		saveData();
	    		Thread.sleep(MediaDataConstants.MAX_SLEEP_TIME);
	    		readData();
	    		rescrapeData();
	    		today = new Date();
	    		scrapeDailyNews(today,today,newspaperRecord);
	    	} catch(InterruptedException ie){
	    		Thread.currentThread().interrupt(); // propagate interrupt
	    		exit = true;
	      	} catch(Exception e) {
	      		exit = true;
			    try {
					throw new Exception (e);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
	      	}
	    }
	    saveData();
	    LOGGER.info(name+" Server is stooped.....");
	}

	private void initNewsFeedData() {
		try {
			String methodName = "getNewspaper";
	        Method getNewspaperMethod = newsObject.getClass().getMethod(methodName);
			newspaperRecord = (NewspaperType) getNewspaperMethod.invoke(newsObject);
			dirFlag = nsdhandler.createDir();
						
			if (dirFlag) {
				// scrape data from failed dates
				readData();
				if (!unscrapingdata.isEmpty()) {
					//avoid duplicate dates
					if (start_date != null && end_date != null) {
						Date start = formatter.parse(start_date);
						Date end = formatter.parse(end_date);
						DateIterator itor = new DateIterator(start, end);
						while (itor.hasNext()) {
							Date date = itor.next();
							String del_date = formatter.format(date);
							if (unscrapingdata.containsKey(del_date)) {
								unscrapingdata.remove(del_date);
							}
						}	
					}
					//check today's data
					if (unscrapingdata.containsKey(formatter.format(today))) todayFlag = false;
					rescrapeData();
				}
			}
			
			if (start_date != null && end_date != null) {
			    //historical data feeding
		    	Date start = formatter.parse(start_date);
		        Date end = formatter.parse(end_date);
		        scrapeDailyNews(start,end,newspaperRecord);
			}
						
			getTodayNews();
		} catch(InterruptedException ie){
			Thread.currentThread().interrupt(); // propagate interrupt
			exit = true;
		} catch(Exception e) {
			exit = true;
		    try {
				throw new Exception (e);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void stopFlag() {
		exit = true;
		String methodName = "stopFlag";
		try {
			Method stopFlagMethod = newsObject.getClass().getMethod(methodName);
			stopFlagMethod.invoke(newsObject);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}

	}

	private void readData() {
		if (dirFlag) {
			LOGGER.info("read data from "+name+".csv");
			try {
				nsdhandler.open(name+".csv");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			unscrapingdata = nsdhandler.getUnscrapingData();
		}
	}

	private void saveData() {
		if (dirFlag) {
			LOGGER.info("save data into "+name+".csv");
			nsdhandler.setUnscrapingData(unscrapingdata);
			try {
				nsdhandler.save(name+".csv");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void rescrapeData() throws Exception {
		//scrape articles from failed dates
		if (!unscrapingdata.isEmpty()) {
			for (String sdate: unscrapingdata.keySet()) {
			//for (int i=0; i<unscrapingdata.size(); i++) {
				if (exit) break;
				Date date = formatter.parse(sdate);
				scrapeDailyNews(date,date,newspaperRecord);
			}
		}
	}
	
	private void getTodayNews() throws Exception {
	
		if (start_date != null && end_date != null && todayFlag) {
			Date end = formatter.parse(end_date);
			if (!end.equals(today)) {
				if (end.before(today)) {
					scrapeDailyNews(today,today,newspaperRecord);
				}
			}
		} else {
			if (todayFlag) scrapeDailyNews(today,today,newspaperRecord);
		}
	}
	
	private void setNewsfeedObject(String feedurl) {
		int idx = feedurl.indexOf(":");
	    name = feedurl.substring(0,idx).trim();
	    String furl = feedurl.substring(idx+1).trim();
	    LOGGER.info("News Prefix: "+name);
	    LOGGER.info("feedURL: "+furl);
	    classname = NewspaperClassNameEnum.valueOf(name).getClassname();
	   
		try {
			today = formatter.parse(formatter.format(new Date()));
			Class<?> clazz = Class.forName(classname);
			Constructor<?> ctor = clazz.getConstructor(String.class);
			newsObject = ctor.newInstance(new Object[] { furl });
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}		
		
	    //return furl;
	}
	
	private void scrapeDailyNews(Date start, Date end, 
			NewspaperType newstype) throws Exception {

		//HefeiDaily hf = new HefeiDaily(feedUrl);
		//Class<?> clazz = Class.forName("edu.sdsc.media.HefeiDaily");
		/*
		Class<?> clazz = Class.forName(classname);
		Constructor<?> ctor = clazz.getConstructor(String.class);
		Object object = ctor.newInstance(new Object[] { feedUrl });		
		*/
		String methodName = "getArticles";
        Class<?>[] paramTypes = {Date.class};
        Method getArticleMethod = newsObject.getClass().getMethod(methodName, paramTypes);
        		
		// Date start = formatter.parse(start_date);
	    // Date end = formatter.parse(end_date);
	    DateIterator itor = new DateIterator(start, end);
		MediaDataRecord mdr = null;
		// NewspaperType newstype = getNewspaper();
		while (itor.hasNext() && !exit) {
			Date date = itor.next();
			LOGGER.info("---------------------------------------------------------------");
			LOGGER.info("Scrape news on "+formatter.format(date));
			String dateStr = formatter.format(date);
			//List<ArticleType> articles = hf.getArticles(date);
			@SuppressWarnings("unchecked")
			List<ArticleType> articles = (List<ArticleType>) getArticleMethod.invoke(newsObject,date);
			LOGGER.info("Article size: "+articles.size());
			if (!articles.isEmpty() && articles.size() > 0) {
				LOGGER.info("Remove date: "+dateStr);
				if (unscrapingdata.containsKey(dateStr)) {
					unscrapingdata.remove(dateStr);
				}		
				for (ArticleType article : articles) {
					if (exit) break;
					mdr = new MediaDataRecord();
					//article.setPubdate(date);
					//newstype.setNewspapername(article.getPapername());
					article.setNewspaper(newstype);
					mdr.setArticle(article);
					mediaFeedBuffer.add(mdr);
				}
			} else {
				LOGGER.info("Add/Replace date: "+dateStr);
				if (!unscrapingdata.containsKey(dateStr)) {
					unscrapingdata.put(dateStr, 0);
				} else {
					int numoftrials = unscrapingdata.get(dateStr).intValue();
					numoftrials++;
					if (numoftrials > MediaDataConstants.MAX_NUM_OF_TRIALS) {
						unscrapingdata.remove(dateStr);
					} else {
						unscrapingdata.replace(dateStr, numoftrials);
					}
				}
			}
		}
	}

}
