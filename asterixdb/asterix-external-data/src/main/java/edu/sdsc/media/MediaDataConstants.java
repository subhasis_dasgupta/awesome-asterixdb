/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

public class MediaDataConstants {

	/**
     * adapter aliases
     */
	public static final String ALIAS_NEWSPAPER_ADAPTER = "newspaper";

	 /**
     * Bulletin record readers
     */
	public static final String READER_NEWSPAPER = "newspaper";
	
	 /**
     * supported bulletin record formats
     */
	public static final String FORMAT_NEWSPAPER = "media";
	
	/**
	 * input parameters
	 */
	public static final String KEY_MEDIA_URL = "url";
	public static final String KEY_START_DATE = "start_date";
	public static final String KEY_END_DATE = "end_date";
	
	/**
	 * feed parameters
	 */
	public static final int MAX_NUM_OF_TRIALS = 10;
	public static final long MAX_SLEEP_TIME = 24*60*60*1000;  //milliseconds
	
    public static class MediaDataType {

    	//Article data type
    	public static final String ARTICLE_ID = "article_id";
        public static final String TITLE = "title";
        public static final String PUBDATE = "pub_date";
        public static final String NEWSTYPE = "newspaper";
        public static final String REPORTER = "reporter";
        public static final String KEYWORDS = "keywords";
        public static final String PLACE = "place_of_report";
        public static final String ORGANIZATION = "organizations";
        public static final String PERSON = "people_mentioned";
        public static final String SECTION = "newspaper_section";
        public static final String PAGEPICURL = "page_pic_url";
        public static final String PAGENUM = "page_num";
        public static final String PAGE = "page";
        public static final String FILE = "file";
        public static final String CONTENT = "content";
        
        // DailyNewsType
        public static final String DAILYNEWD_ID = "dailynews_id";
        public static final String PUB_DATE = "pub_date";
        public static final String ARTICLES = "articles";
        
        // NewspaperType 
        public static final String NEWSPAPER_ID = "newspaper_id";
        public static final String NEWSPAPER_NAME = "newspaper_name";
        public static final String LOCATION = "location";
        public static final String IS_COMMERCIAL = "is_commercial";
        public static final String IS_LOCAL = "is_local_government_issued";
        public static final String IS_FEDERAL = "is_federal_government_issued";
        public static final String CIRCULATION = "circulation_level";
        public static final String START_DATE = "start_date";
        public static final String END_DATE = "end_date";
        public static final String DAILYNEWS = "dailynews";
        
        // AuthorType
        public static final String AUTHOR_NAME = "name";
        public static final String AUTHOR_AREA =  "specialty";

        // LocationNameType
        public static final String LN_NAME = "name";
        public static final String LN_TYPE = "location_type";
        public static final String LN_INO = "in_ontology";
        public static final String LN_ONID = "ontology_id";
        public static final String LN_ONEID = "ontology_entity_id";

        // OrganizationType
        public static final String ORG_NAME = "name";
        public static final String ORG_INO = "in_ontology";
        public static final String ORG_ONID = "ontology_id";
        public static final String ORG_ONEID = "ontology_entity_id";

        // PersonType
        public static final String PER_NAME = "name";
        public static final String PER_INO = "in_ontology";
        public static final String PER_ONID = "ontology_id";
        public static final String PER_ONEID = "ontology_entity_id";

        // AOrderedListType, AUnorderedListType
        public static final String AOLIST_DAILYNEWS = "NewspaperType_dailynews";
        public static final String AOLIST_ARTICLE = "DailyNewsType_articles";
        public static final String AOLIST_SPECIALITY = "AuthorType_specialty";
        public static final String AOLIST_PLACE = "ArticleType_place_of_report";
        public static final String AOLIST_LOCATION = "NewspaperType_location";
        public static final String AULIST_REPORTER = "ArticleType_reporter";
        public static final String AULIST_KEYWORD = "ArticleType_keywords";
        public static final String AULIST_ORG = "ArticleType_organizations";
        public static final String AULIST_PERSON = "ArticleType_people";
    }

}
