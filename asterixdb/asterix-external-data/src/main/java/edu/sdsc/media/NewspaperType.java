/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.util.Date;

public class NewspaperType {
	
	private DailyNewsType[] dailynews;
	private long newspaperid;
    private String newspapername;
    private LocationNameType[] location;
    private boolean iscommercial;
    private boolean islocalgovernmentissued;
    private boolean isfederalgovernmentissued;
    private String circulationlevel;
    private Date startdate;
    private Date enddate;

    public NewspaperType () {
    	//empty constructor
    }
	
    public void setNewspaperid(long newspaperid) {
    	this.newspaperid = newspaperid;
    }
    public long getNewspaperid() {
    	return this.newspaperid;
    }
    
    public void setDailynews(DailyNewsType[] dailynews) {
    	this.dailynews = dailynews;
    }
    public DailyNewsType[] getDailynews() {
    	return this.dailynews;
    }
    public void setNewspapername(String newspapername) {
    	this.newspapername = newspapername;
    }
    public String getNewspapername() {
    	return this.newspapername;
    }
    
	public void setLocation(LocationNameType[] location) {
		this.location = location;
	}
	public LocationNameType[] getLocation() {
		return this.location;
	}
	
	public void setIscommercial(boolean iscommercial) {
		this.iscommercial = iscommercial;
	}
	public boolean isIscommercial() {
		return this.iscommercial;
	}
	
	public void setIslocalgovernmentissued(boolean islocalgovernmentissued) {
		this.islocalgovernmentissued = islocalgovernmentissued;
	}
	public boolean isIslocalgovernmentissued() {
		return this.islocalgovernmentissued;
	}
	
	public void setIsfederalgovernmentissued(boolean isfederalgovernmentissued) {
		this.isfederalgovernmentissued = isfederalgovernmentissued;
	}
	public boolean isIsfederalgovernmentissued() {
		return this.isfederalgovernmentissued;
	}
	
	public void setCirculationlevel(String circulationlevel) {
		this.circulationlevel = circulationlevel;
	}
	public String getCirculationlevel() {
		return this.circulationlevel;
	}
	
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getStartdate() {
		return this.startdate;
	}
	
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public Date getEnddate() {
		return this.enddate;
	}


}
