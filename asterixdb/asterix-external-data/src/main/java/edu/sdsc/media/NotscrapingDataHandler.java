/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class NotscrapingDataHandler {
	private static final Logger LOGGER = Logger.getLogger(NotscrapingDataHandler.class.getName());
    //private Map<String, Integer> _map = new HashMap<String, Integer>();
    private Map<String, Integer> _map = new ConcurrentHashMap<String, Integer>();
    private List<Map<String, Integer>> scrape_dates = new ArrayList<Map<String, Integer>>();
    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private final String home_path;

    public NotscrapingDataHandler() {
    	home_path = System.getProperty("user.home") + File.separator + "AsterixDBNewspaperFeedAdaptor";
    }

    public void open(String filename) throws FileNotFoundException, IOException {
        open(filename, ',');
    }
 
    public void open(String filename, char delimiter) throws FileNotFoundException, IOException {
    	File file = new File(home_path+File.separator+filename);
    	if (file.exists()) {
    		Scanner scanner = new Scanner(file);
    		scanner.useDelimiter(Character.toString(delimiter));
    		clear();
 
    		while(scanner.hasNextLine()) {
    			String[] values = scanner.nextLine().split(Character.toString(delimiter));
    			LOGGER.info(values[0]+delimiter+values[1]);
    			_map.put(values[0], new Integer(values[1]));
    			//scrape_dates.add(_map);
    		}    			
    		scanner.close();
    	} else {
    		LOGGER.info("The file, "+home_path+File.separator+filename+" is not existed.");
    	}
        //String today = formatter.format(new java.util.Date());
        //_map.put(today,0);
    }

    public boolean createDir() throws FileNotFoundException, IOException {
        boolean flag = true;
        File feeddatafile = new File(home_path);

        if (feeddatafile.exists()) {
        	LOGGER.info(feeddatafile + " already exists");
        } else if (feeddatafile.mkdirs()) {
            LOGGER.info(feeddatafile + " was created");
        } else {
            flag = false;
            LOGGER.info(feeddatafile + " was not created");
        }
        return flag;
    }
 
    public void setUnscrapingData(Map<String, Integer> _map) {
    	this._map = _map;
    }
    public Map<String, Integer> getUnscrapingData() {
    	return this._map;
    }
    
    public void save(String filename) throws IOException {
        save(filename, ',');
    }
 
    public void save(String filename, char delimiter) throws IOException {
        File file = new File(home_path+File.separator+filename);
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
 
        for (String key: _map.keySet()) {
            int numoftrials = _map.get(key).intValue();
            bw.write(key);
            bw.write(delimiter);
            bw.write(String.valueOf(numoftrials));
            bw.newLine();
            LOGGER.info(key+delimiter+numoftrials);
        }

        bw.flush();
        bw.close();
    }
  
    public void clear() {
        _map.clear();
    }
 

}
