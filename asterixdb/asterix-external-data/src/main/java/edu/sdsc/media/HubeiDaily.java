/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package edu.sdsc.media;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.net.*;
import java.text.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.HttpStatusException;

public class HubeiDaily implements Newspaper {
	private static final Logger LOGGER = Logger.getLogger(HubeiDaily.class.getName());
    private static DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
    private static DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
    //Change ROOT_URL
    private static String ROOT_URL = "http://hbrb.cnhubei.com/HTML/hbrb/";
    private static String IMG_URL = "http://hbrb.cnhubei.com/hbrb/";
    private volatile boolean exit = false;

	public HubeiDaily () {
		//empty constructor
	}

	public HubeiDaily (String url) {
		ROOT_URL = url;
	}

    public void stopFlag() {
		exit = true;
	}
    
	public NewspaperType getNewspaper() throws Exception {		
		//NewspaperType
		NewspaperType newstype = new NewspaperType();
		newstype.setNewspaperid(8);
		newstype.setCirculationlevel("Daily");
		LocationNameType[] location = new LocationNameType[1];
		location[0] = new LocationNameType();
		location[0].setName("湖北");
		location[0].setLocationtype("Province");
		location[0].setInontology(false);
		location[0].setOntologyid(0);
		location[0].setOntologyentityid(0);
		newstype.setLocation(location);
		newstype.setNewspapername("湖北日报");
		newstype.setStartdate(formatter2.parse("1990-01-02"));
		newstype.setEnddate(formatter2.parse("2030-12-31"));
		newstype.setIscommercial(false);
		newstype.setIsfederalgovernmentissued(true);
		newstype.setIslocalgovernmentissued(true);
		
		return newstype;
	}

    public static void main(String[] args) throws Exception {
	
	Date start = formatter2.parse("2014-04-29");
	Date end = formatter2.parse("2014-04-30");
	DateIterator itor = new DateIterator(start, end);
	//Change name
	HubeiDaily hf = new HubeiDaily();
	while (itor.hasNext()) {
	    Date date = itor.next();
	    System.out.println("---------------------------------------------------------------");
	    System.out.println("Scrape news on "+formatter2.format(date));
	    List<ArticleType> articles = hf.getArticles(date);
	    for (ArticleType article : articles) {
		System.out.println("===============================");
		System.out.println(article);
	    }
	}
    }


    public List<ArticleType> getArticles(Date date) throws Exception {

	List<ArticleType> articles = new ArrayList<ArticleType>();
	//Change node format
	for (int i=1; i<=16; i++) {
		if (exit) break;
	    String url = (i == 1) ? "index.html" : "hbrb"+i+".html";
	    LOGGER.info("------------------------");
	    LOGGER.info(ROOT_URL+formatter.format(date)+"/"+url);
	    scrapePage(date, url, articles);
	}

	return articles;
    }


    public void scrapePage(Date date, String url, List<ArticleType> articles) throws Exception{

	try {
	    Document doc = Jsoup.connect(ROOT_URL+formatter.format(date)+"/"+url)
		.userAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)")
		.referrer("www.test.com")
		.timeout(30000) 
		.get();

	    //System.out.println(doc.html());

	    // find click picture
	    Element pic = doc.select("img[usemap]").first();
	    String picurl = pic.attr("src");
	    if (picurl != null) {
		int pos = picurl.lastIndexOf("/");
		picurl = IMG_URL+formatter.format(date)+"/"+picurl.substring(pos+1);
	    }

	    // find all content links
	    Elements maps = doc.select("map");
	    List<String> urls = new ArrayList<String>();
	    for (Element map : maps) { 
	    	if (exit) break;
		String areaText = map.html();
		//System.out.println(areaText);

		int pos = areaText.indexOf("href=\"");
		while (pos != -1 && !exit) {
		    areaText = areaText.substring(pos+6);
		    pos = areaText.indexOf("\">");
		    String aURL = areaText.substring(0, pos);
		    //System.out.println(aURL);
		    urls.add(aURL);

		    areaText = areaText.substring(pos+2);
		    pos = areaText.indexOf("href=\"");
		}
	    }

	    for (String aURL : urls) {
	    	if (exit) break;
		boolean success = false;
		while (!success && !exit) {
		    try {
			scrapeContent(date, aURL, articles);
			success = true;
		    } catch (SocketTimeoutException ex) {
		    	LOGGER.info("Error Message: "+ex.getMessage());
		    	success = false;
		    }
		}
	    }

	    for (ArticleType article : articles) {
	    	if (exit) break;
		article.setPagepicurl(picurl);
	    }

	} catch (Exception ex) {
	    LOGGER.info("Error Message: "+ex.getMessage());
	}

    }


    public void scrapeContent(Date date, String url, List<ArticleType> articles) throws Exception{

	LOGGER.info("==============================================");
	LOGGER.info("loading article at "+ROOT_URL+formatter.format(date)+"/"+url);
    try {
	Document doc = Jsoup.connect(ROOT_URL+formatter.format(date)+"/"+url)
	    .userAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)")
	    .referrer("www.test.com")
	    .timeout(30000) 
	    .get();
	
	//System.out.println(doc.html());
	//Change this for each newspaper

	Elements contents = doc.select("div#copytext");
	String contentStr = "";
	for (Element content : contents) {
		if (exit) break;
	    String txt = content.text().replaceAll(" ", "").replaceAll("□", " ").replaceAll(""," ").replaceAll("《", " ").replaceAll("》", " ").replaceAll("【", "").replaceAll("】", "");
	    //System.out.println("content: "+txt);

	    // segment and tag the content
	    //System.out.println("-------------------------");
	    contentStr = StanfordNERProcessor.process(txt);
	    //System.out.println(contentStr);	    

	}

	// get pagenum
	Element td = doc.select("table#Table16").select("td").first();
	String pagenum = td.text();

	// get date
	String dateStr = formatter2.format(date);

	// get author
	String author = null;

	// get title
	Element titleElement = doc.select("title").first();
	String title = titleElement.text();
	int pos = title.indexOf("_湖北日报_多媒体报");
	title = title.substring(0, pos);

	// get keywords
	String keyword = null;

	// get pagepicurl
	String pagepicurl = "";
	
	// get newspaper name
	String papername = "湖北日报";

	// get page
	String page = "";

	String file = ROOT_URL+formatter.format(date)+"/"+url;
	file = "Hubei/"+file.replaceAll("/", "_")+".txt";

	Date articleDate = null;
	articleDate = formatter2.parse(dateStr);
	ArticleType article = new ArticleType(title, contentStr, articleDate);
	AuthorType[] reporter = new AuthorType[1];
	reporter[0] = new AuthorType();
	reporter[0].setName(author);
	reporter[0].setSpecialty(null);
	article.setReporter(reporter);
	String[] keywords = new String[1];
	keywords[0] = keyword;
	article.setKeywords(keywords);
	article.setPagepicurl(pagepicurl);	
	article.setPagenum(pagenum);	
	article.setPapername(papername);	
	article.setPage(page);	
	article.setFile(file);	

	if (!contentStr.trim().equals("") && !contains(articles, article) && !title.equals("") ) {
		if (contentStr.length() > 5) {
			Pattern p = Pattern.compile("<(\\w+)>(.*?)</\\1>");
	        Matcher m = p.matcher(contentStr);

			//filtering content
	        List<String> orgs = new ArrayList<String>();
	        List<String> people = new ArrayList<String>();
	        List<String> locs = new ArrayList<String>();
	        String tag = null;
	        String value = null;
	        while (m.find()) {
	        	tag = m.group(1);
	        	value = m.group(2);
	            // System.out.println(tag+" : " + value);
	            switch (tag) {
	            	case "LOC":
	            		locs.add(value);
	            		break;
	            	case "PERSON":
	            		people.add(value);
	            		break;
	            	case "ORG":
	            		orgs.add(value);
	            		break;
	            	default:
	            		break;
	            }
	        }
	        
	        if (!orgs.isEmpty()) {
	        	int org_size = orgs.size();
	        	OrganizationType orgtype[] = new OrganizationType[org_size];
	        	for (int i=0; i<org_size; i++) {
	        		orgtype[i] = new OrganizationType();
	        		orgtype[i].setName(orgs.get(i));
	        	}
	        	article.setOrganizations(orgtype);
	        }
	        if (!locs.isEmpty()) {
	        	int loc_size = locs.size();
	        	LocationNameType loctype[] = new LocationNameType[loc_size];
	        	for (int i=0; i<loc_size; i++) {
	        		loctype[i] = new LocationNameType();
	        		loctype[i].setName(locs.get(i));
	        		loctype[i].setLocationtype("Unknown");
	        	}
	        	article.setPlaceofreport(loctype);
	        }
	        if (!people.isEmpty()) {
	        	int per_size = people.size();
	        	PersonType persontype[] = new PersonType[per_size];
	        	for (int i=0; i<per_size; i++) {
	        		persontype[i] = new PersonType();
	        		persontype[i].setName(people.get(i));
	        	}
	        	article.setPeople(persontype);
	        }
		}
		articles.add(article);
	}
    } catch (Exception ex) {
    	LOGGER.info("Error Message: "+ex.getMessage());
    }
    }



    static public boolean contains(List<ArticleType> articles, ArticleType article) {

	for (ArticleType tmp : articles) {
	    String tmpTitle = tmp.getTitle();
	    String title = article.getTitle();

	    String tmpContent = tmp.getContent();
	    String content = article.getContent();

	    if (tmpTitle != null && title != null && tmpTitle.equals(title) &&
		tmpContent != null && content != null && tmpContent.equals(content)) {
		return true;
	    }
	}

	return false;

    }

    
    static public String getValue(String text, String attName) {

	int pos1 = text.indexOf("<"+attName+">");
	int pos2 = text.indexOf("</"+attName+">");
	if (pos1 != -1 && pos2 != -1) {
	    return text.substring(pos1+attName.length()+2, pos2).trim();
	}
	return "";
    }



}

