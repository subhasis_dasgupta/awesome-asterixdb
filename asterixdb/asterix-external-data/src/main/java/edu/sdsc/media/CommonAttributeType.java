/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

public class CommonAttributeType {
	private String name;
    private boolean inontology = false;
    private long ontologyid = 0;
    private long ontologyentityid = 0;
    
    public CommonAttributeType() {
    	//empty constructor
    }

    public void setName(String name) {
    	this.name = name;
    }
    public String getName() {
    	return this.name;
    }
        
    public void setInontology(boolean inontology) {
    	this.inontology = inontology;
    }
    public boolean isInontology() {
    	return this.inontology;
    }
    
    public void setOntologyid(long ontologyid) {
    	this.ontologyid = ontologyid;
    }
    public long getOntologyid() {
    	return this.ontologyid;
    }
    
    public void setOntologyentityid(long ontologyentityid) {
    	this.ontologyentityid = ontologyentityid;
    }
    public long getOntologyentityid() {
    	return this.ontologyentityid;
    }

}
