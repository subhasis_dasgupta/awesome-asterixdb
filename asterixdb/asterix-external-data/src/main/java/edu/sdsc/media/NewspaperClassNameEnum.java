/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

public enum NewspaperClassNameEnum {
	
	HefeiDaily("edu.sdsc.media.HefeiDaily"),
	ChangchunDaily("edu.sdsc.media.ChangchunDaily"),
	ChifengDaily("edu.sdsc.media.ChifengDaily"),
	GuilinDaily("edu.sdsc.media.GuilinDaily"),
	LanZhouDaily("edu.sdsc.media.LanZhouDaily"),
	JinchengDaily("edu.sdsc.media.JinchengDaily"),
	//ChengdeDaily("edu.sdsc.media.ChengdeDaily"),
	GuangzhouDaily("edu.sdsc.media.GuangzhouDaily"),
	HenyangDaily("edu.sdsc.media.HenyangDaily"),
	HubeiDaily("edu.sdsc.media.HubeiDaily"),
	FuzhouDaily("edu.sdsc.media.FuzhouDaily")
	;
	
	private final String classname;

	NewspaperClassNameEnum(String classname) {
        this.classname = classname;
    }

    public String getClassname() {
        return this.classname;
    }
}
