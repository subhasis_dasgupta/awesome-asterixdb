/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.sdsc.media;

import java.util.List;
import java.util.Map;
import java.util.Date;

public class MediaDataRecord {

	private Map<Date,List<ArticleType>> dailynews;
	private NewspaperType newspaper;
	private ArticleType article;
	
    public MediaDataRecord () {
    	//empty constructor
    }
	
    public void setDailynews(Map<Date,List<ArticleType>> dailynews) {
    	this.dailynews = dailynews;
    }
    public Map<Date,List<ArticleType>> getDailynews() {
    	return this.dailynews;
    }

    public void setNewspaper(NewspaperType newspaper) {
    	this.newspaper = newspaper;
    }
    public NewspaperType getNewspaper() {
    	return this.newspaper;
    }
    
    public void setArticle(ArticleType article) {
    	this.article = article;
    } 
    public ArticleType getArticle() {
    	return this.article;
    }
   
}
